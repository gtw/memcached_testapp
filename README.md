# memcached_testapp

Working `config.py` file for `sigmetrics-tool`:

```
# SSH Configurations
workers = [...]
user = ...
keyfile = ...
port_num = 22

# Experiemnts Repo
repo = "https://gitlab.flux.utah.edu/Hannnah1/memcached_testapp"

# Filepaths and commands
init_script_call = "bash memcached_testapp/initialize.sh"
experiment_script_call = "bash memcached_testapp/exp_config.sh"
results_dir = "memcached_testapp/results"
results_file = "results.txt"

# Options
verbose = True
reboot = False
seed = None
```
