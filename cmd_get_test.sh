#!/bin/bash

trap "kill %1" EXIT

raw_results="results/cmd_get"

./mc-crusher/mc-crusher --conf ./mc-crusher/conf/slab_rebal_torture &

./mc-crusher/bench-sample 127.0.0.1:11211 1 60 cmd_get > $raw_results 2>&1

benchmark_results_dest="results/results.txt"
gawk 'match($0,/\[([0-9.]+)\]/,m) { print m[1]; }' < $raw_results >> $benchmark_results_dest
