"""
SSH Configurations
"""
workers = ["worker"]
user = "ordersage"
keyfile = "/local/id_rsa"
port_num = 22

"""
Experiemnt Repo
"""
repo = "https://gitlab.flux.utah.edu/gtw/memcached_testapp.git"

"""
Filepaths and commands on worker node
"""
init_script_call = "bash memcached_testapp/initialize.sh"
exp_script_call = "bash memcached_testapp/exp_config.sh"
results_dir = "~/memcached_testapp/results"
results_file = "results.txt"

"""
Controller options
"""
# specifies the number of runs for both fixed and random order
# NB: n_runs = 3 should complete all execution in under an hour and
# is suitable for validation of the environment; n_runs = 50 was
# used to generate the results in the Usenix ATC '23 paper and took
# many hours to complete.
n_runs = 3
# specifies if random and fixed runs should be interleaved or not
interleave = True
# prints STDOUT of workers to console
verbose = True
# Ignore reset command for debugging purposes
reset = False
# Set your own random seed
seed = None

"""
Instrumentation options, in the order they need to be added to the experiment
"""
instrumentation_modules = []
